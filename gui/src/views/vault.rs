use crate::{common::Session, theme, ViewTrait};
use heimdallr::Vault;
use iced::{button, Button, Column, Container, Element, Length, Text};
use iced_aw::{floating_button, FloatingButton, Icon, ICON_FONT};

#[derive(Clone, Debug)]
pub enum Message {
    LockVault,
}

#[derive(Default)]
pub struct VaultView {
    vault: String,
    lock_button_state: button::State,
}

impl VaultView {
    pub fn new(vault: String) -> Self {
        Self {
            vault,
            ..Default::default()
        }
    }
}

impl ViewTrait for VaultView {
    type Message = Message;

    fn update(&mut self, session: &mut Session, message: Message) -> Option<crate::Message> {
        match message {
            Message::LockVault => return Some(crate::Message::ChangeToHomeView),
        }
    }

    fn view(&mut self, session: &mut Session) -> Element<Message> {
        let file_name = &session.unlocked[&self.vault]
            .file
            .path()
            .unwrap()
            .file_name()
            .unwrap()
            .to_string_lossy()
            .to_string();

        let column = Column::new().push(Text::new(file_name));

        let content = FloatingButton::new(
            &mut self.lock_button_state,
            Container::new(column)
                //.style(theme::dark::MainContainer)
                .padding(10)
                .width(Length::Fill)
                .height(Length::Fill),
            |state| {
                Button::new(
                    state,
                    Text::new("Lock")
                        .width(Length::Shrink)
                        .height(Length::Shrink)
                        .size(39),
                )
                .on_press(Message::LockVault)
                .padding(5)
                .style(theme::RoundedButton)
            },
        )
        .anchor(floating_button::Anchor::SouthEast)
        .offset(20.0)
        .hide(false)
        .into();

        content
    }
}
