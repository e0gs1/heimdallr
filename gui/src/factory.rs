use iced::{pick_list, PickList};

pub enum Message {

}

pub trait Element {
    fn update(&mut self, message: Message);
    fn view(&mut self) -> iced::Element<Message>;
}

#[derive(Default)]
pub struct UiFactory {
    pick_list_states: Vec<pick_list::State<u8>>    
}

impl UiFactory {
    fn new_pick_list<T, M>(&mut self, options: Vec<T>, selected: Option<T>, on_selected: M) -> PickList<[T], M>
        where [T]: ToOwned + Sized {
        self.pick_list_states.push(pick_list::State::default());
        PickList::new(
            self.pick_list_states.last().unwrap(),
            options,
            selected,
            on_selected
        )
    }
}
