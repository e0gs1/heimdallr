use heimdallr::{AuthType, SodiumArray, Vault};
use std::path::{Path, PathBuf};

pub fn create_heimdallr_auth(factor: u8, value: SodiumArray) -> AuthType {
    if factor == 1 {
        AuthType::Password(value)
    } else if factor == 2 {
        AuthType::UsbKey(value)
    } else {
        AuthType::None
    }
}

pub fn create_vault(path: PathBuf, authenticators: [(u8, SodiumArray); 2]) -> Vault<'static> {
    let [(f1, psw1), (f2, psw2)] = authenticators;
    let authenticators = [
        create_heimdallr_auth(f1, psw1),
        create_heimdallr_auth(f2, psw2),
    ];

    heimdallr::Vault::create(path, &authenticators).unwrap()
}

pub fn factor_type_to_id(factor: &str) -> u8 {
    match factor {
        "Password" => 1,
        "Usb drive" => 2,
        "Phone" => 3,
        _ => 0,
    }
}

pub fn factor_type_id_to_str(factor: u8) -> &'static str {
    match factor {
        1 => "Password",
        2 => "Usb drive",
        3 => "Phone",
        _ => "None",
    }
}
