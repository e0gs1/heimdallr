use crate::cipher::{self, create_iv, create_salt, Cipher, StringCipher};
use crate::database::CipherFile;
use crate::database::Database;
use crate::sodium::SodiumArray;
use crate::AuthType;
use crate::Entry;
use std::convert::TryInto;
use std::io;
use std::path::PathBuf;
use zeroize::Zeroize;

pub fn concat_authenticators(authenticators: &[AuthType; 2]) -> SodiumArray {
    let mut password = String::new();
    for auth in authenticators.iter() {
        match auth {
            AuthType::Password(raw_password) => {
                raw_password.read_unlock();
                password += raw_password.as_str();
                raw_password.lock();
            }
            _ => {}
        }
    }
    let password = SodiumArray::new(password).unwrap();
    password
}

pub struct Vault<'a> {
    pub file: CipherFile<'a, std::fs::File>,
    pub unlocked: Option<SodiumArray>,
}

impl<'a> Default for Vault<'a> {
    fn default() -> Self {
        Self {
            file: CipherFile::None,
            unlocked: None,
        }
    }
}

impl<'a> Vault<'a> {
    pub fn create(path: PathBuf, authenticators: &[AuthType; 2]) -> io::Result<Self> {
        let password = concat_authenticators(authenticators);
        let factors = [authenticators[0].to_u8(), authenticators[1].to_u8()];
        let salt = create_salt(32);
        let iv = create_iv(12);

        let cipher = Cipher::new(&password, &salt, &iv);
        // TODO, revisit and possibly change string_cipher usage
        let string_cipher = StringCipher::new();
        let mut database = Database::new();
        database.unsafe_set("string_cipher", &string_cipher);
        let file = CipherFile::new(path, database, cipher, string_cipher, factors);

        Ok(Vault {
            file,
            unlocked: Some(password),
        })
    }

    pub fn open(path: PathBuf) -> std::io::Result<Self> {
        let file = CipherFile::open(path)?;

        Ok(Vault {
            file,
            unlocked: None,
        })
    }

    pub fn save(&self) -> std::io::Result<()> {
        if let Some(ref password) = self.unlocked {
            self.file.save(&password)?;
        }

        Ok(())
    }

    pub fn lock(&mut self) {
        // Drop SodiumArray which zeroize memory block when being freed by libsodium_free
        self.unlocked = None;
    }

    pub fn unlock(&mut self, authenticators: &[AuthType; 2]) -> io::Result<()> {
        let password = concat_authenticators(authenticators);
        match self.file.decrypt(&password) {
            Ok(file) => {
                self.file = file;
                self.unlocked = Some(password);
                Ok(())
            }
            Err(e) => Err(e),
        }
    }

    pub fn factors(&self) -> [u8; 2] {
        self.file.factors()
    }

    pub fn encrypt_string(&self, data: String) -> Vec<u8> {
        Vec::new()
    }

    pub fn add_group(&mut self, parent: Option<&str>, name: String) {
        if let CipherFile::Uncrypted {
            ref mut database, ..
        } = self.file
        {
            let parent = if let Some(p) = parent { p } else { "root" };

            database.set_group(Some(parent), name);
        }
    }

    pub fn get_group(&self, name: &str) -> Option<Vec<String>> {
        match self.file {
            CipherFile::Uncrypted { ref database, .. } => database.get_group(name),
            _ => None,
        }
    }

    pub fn new_entry(&mut self, group: Option<&str>, entry: Entry) {
        if let CipherFile::Uncrypted {
            ref mut database, ..
        } = self.file
        {
            database.set_entry(group, entry)
        }
    }

    pub fn get_entry(&self, key: &str) -> Option<Entry> {
        if let CipherFile::Uncrypted { ref database, .. } = self.file {
            return database.get_entry(key);
        }

        None
    }

    pub fn delete_entry(&mut self, group: Option<&str>, key: &str) {
        if let CipherFile::Uncrypted {
            ref mut database, ..
        } = self.file
        {
            database.delete_entry(group, key)
        }
    }
}
